var nav = $('#menu');

$(window).scroll(function(){
  if($(this).scrollTop() > 50){
    nav.addClass('scrolled');
  } else {
    nav.removeClass('scrolled');
  }
});

var $doc = $('html, body');
$('a').click(function() {
  $doc.animate({
    scrollTop: $( $.attr(this, 'href') ).offset().top
  }, 500);
  return false;
});

$('#computers').click(function() {
  $('#computers-text').toggleClass('text-open text-closed');
});

$('#equipments').click(function() {
  $('#equipments-text').toggleClass('text-open text-closed');
});

$('#components').click(function() {
  $('#components-text').toggleClass('text-open text-closed');
});
